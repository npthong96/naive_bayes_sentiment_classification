"""
Read vocabularies from dataset
Remove stop-words and rare words
"""
import utils

pos_folder = './data/review_polarity/txt_sentoken/pos/'
neg_folder = './data/review_polarity/txt_sentoken/neg/'

## read list of positive files
pos_files = utils.get_files_path_in_folder(pos_folder)
## set 80% of files as train set
pos_files = pos_files[:int(len(pos_files)*0.8)]
## read list of negative files
neg_files = utils.get_files_path_in_folder(neg_folder)
## set 80% of files as train set
neg_files = neg_files[:int(len(neg_files)*0.8)]
## join them
corpus_files = pos_files + neg_files

## build vocabularies dict from corpus files
## and count frequence of each vocabulary
## vocabs = {word: frequence}
vocabs = {}
for f in corpus_files:
    tokens = utils.get_tokens_in_file(f)
    if tokens is not None:
        for token in tokens:
            if token not in vocabs:
                vocabs[token] = 1
            else:
                vocabs[token] += 1

## keep only vocabularies which frequence >= 2 and have long >= 2
## remove stop words, top words are K highest frequence words
K_top           = 50
min_frequence   = 2
min_length      = 2

word_frequences = [frequence for word, frequence in vocabs.items()]
word_frequences.sort()
min_stop_word_frequences = word_frequences[-K_top]
filtered_vocabs = {word: frequence for word, frequence in vocabs.items()\
                     if frequence < min_stop_word_frequences\
                     and frequence >= min_frequence\
                     and len(word) >= min_length}
print('Removed stop words, length of vocabs: ', len(filtered_vocabs))

## save vocabularies to file
vocab_path = './data/vocabs.txt'
with open(vocab_path, "w") as f:
    vocabs_string = '\n'.join(filtered_vocabs)
    f.write(vocabs_string)
    print('Wrote vocabs file at ', vocab_path)
f.close()
