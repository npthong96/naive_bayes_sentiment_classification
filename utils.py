"""
Common functions
"""

import os
from os.path import isfile, splitext

def get_files_path_in_folder(parent_folder_path, extension_filter=['.txt']):
    """
    Get files path in a folder (not includes sub-folders)
    Parameters:
        @parent_folder_path: path of containing folder
        @extension_file: list of accepted file extensions
    Returns:
        files: list of file's paths in parent_folder_path
    """
    files = []
    for f in os.listdir(parent_folder_path):
        full_path = parent_folder_path + f
        if isfile(full_path) and splitext(full_path)[-1] in extension_filter:
            files.append(full_path)
    return files 
    
def get_tokens_in_file(file_path):
    """
    Get tokens in file, token is a vocabulary
    Parameters:
        @file_path: path of file to read
    Returns:
        @tokens: tokens of text read in file
    """
    tokens = None
    with open(file_path, "r") as f:
        f = open(file_path, "r")
        sent = f.read()
        tokens = sent.split()
        tokens = [token.lower() for token in tokens if token.isalpha() and len(token) >= 2]
    f.close()
    return tokens
    
def read_vocabs(vocab_file_path):
    """
    Read vocabularies in vocabs.txt file
    Parameters:
        @vocab_file_path: path of vocabs.txt
    Returns:
        @vocabs: list of vocabularies
    """
    vocabs = None
    with open(vocab_file_path,"r") as f:
        vocabs = f.read().split('\n')
    f.close()
    return vocabs
    
def read_number_of_samples(file_path):
    """
    Read number of positive samples, negative samples,
    positive vocabularies, negative vocabularies save
    in  number_of_sample.txt file
    Parameters:
        @file_path: path of number_of_sample.txt
    Returns:
        number of positive samples, negative samples,
        positive vocabularies, negative vocabularies`
    """
    pos_samples = None
    neg_samples = None
    with open(file_path, 'r') as f:
        data = f.read().split()
        pos_samples = int(data[0])
        neg_samples = int(data[1])
        pos_vocabs  = int(data[2])
        neg_vocabs  = int(data[3])
    f.close()
    return pos_samples, neg_samples, pos_vocabs, neg_vocabs
    
def read_vocab_frequence(vocabs, file_path):
    """
    Read frequences of vocabularies saved in pos_frequences.txt and neg_frequences.txt
    Parameters:
        @vocabs: vocabularies list
        @file_path: file to pos_frequences.txt or neg_frequences.txt
    Returns:
        @frequences: frequences of vocabularies, frequences = {vocabulary: frequence}
    """
    frequences = {}
    with open(file_path, 'r') as f:
        data = f.read().split('\n')
        for vocab, frequence in zip(vocabs, data):
            frequences[vocab] = int(frequence)
    f.close()
    return frequences
    
    
