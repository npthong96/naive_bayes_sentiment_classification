"""
Class for predict new sentence
"""

import numpy as np
import utils

vocabs_path         = './data/vocabs.txt'
pos_frequences_path = './data/pos_frequences.txt'
neg_frequences_path = './data/neg_frequences.txt'
num_samples_path    = './data/number_of_sample.txt'

class NaiveBayesSentiment:
    def __init__(self):
        self.__vocabs = utils.read_vocabs(vocabs_path)
        self.__pos_samples, self.__neg_samples, self.__pos_vocabs, self.__neg_vocabs = utils.read_number_of_samples(num_samples_path)
        self.__pos_frequences = utils.read_vocab_frequence(self.__vocabs, pos_frequences_path)
        self.__neg_frequences = utils.read_vocab_frequence(self.__vocabs, neg_frequences_path)

    def predict(self, sent):
        """
        Predict class of new sentence
        Parameters:
            @sent: sentence to predict, example "I don't like it"
        Returns:
            +1 if sentence is positive sentiment
            -1 if sentence is negative sentiment
        """
        tokens = [token.lower() for token in sent.split() if token.lower().isalpha() and token.lower() in self.__vocabs]
        
        p_pos = np.log(self.__pos_samples/(self.__pos_samples + self.__neg_samples))
        p_neg = np.log(self.__neg_samples/(self.__pos_samples + self.__neg_samples))

        for token in tokens:
            p_pos += np.log((self.__pos_frequences[token]+1)/(self.__pos_vocabs + len(self.__vocabs)))
            p_neg += np.log((self.__neg_frequences[token]+1)/(self.__neg_vocabs + len(self.__vocabs)))
        
        if p_pos > p_neg:
            return 1
        return -1

if __name__ == '__main__':
    model = NaiveBayesSentiment()
    cls = model.predict('the film are much awful good boring')
    print(cls)


