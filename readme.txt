-Run on python3
-First, extract dataset in ./data folder
-Do following steps:
	+Build vocabularies: python build_vocabs.py
	+Build vocabs: python build_frequences.py
	+Test model: python test.py
-To predict new sentence: use class in predict.py
