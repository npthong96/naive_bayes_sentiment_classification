"""
Evaluate model
"""
import numpy as np
import utils
import predict

pos_folder = './data/review_polarity/txt_sentoken/pos/'
neg_folder = './data/review_polarity/txt_sentoken/neg/'

## read list of positive files
pos_files = utils.get_files_path_in_folder(pos_folder)
## set 80% of files as train set and 20% as test set
pos_test_files  = pos_files[int(len(pos_files)*0.8):]
## read list of negative files
neg_files = utils.get_files_path_in_folder(neg_folder)
## set 80% of files as train set and 20% as test set
neg_test_files  = neg_files[int(len(neg_files)*0.8):]
## join them
labels     = [1 for _ in range(len(pos_test_files))] + [-1 for _ in range(len(neg_test_files))]
test_files = pos_test_files + neg_test_files

## evaluate
wrong_counter = 0
model = predict.NaiveBayesSentiment()

for i, f in enumerate(test_files):
    with open(f,'r') as f:
        sent = f.read()
        clf = model.predict(sent)
        if clf != labels[i]:
            wrong_counter += 1
    f.close()

err_rate = wrong_counter/len(labels)
print('Accurate: ', 1-err_rate)
