"""
Counting frequences of vocabylaries
Vocabularies are gotten by build_vocabs.py
and save to file vocabs.txt
"""

import utils

pos_folder = './data/review_polarity/txt_sentoken/pos/'
neg_folder = './data/review_polarity/txt_sentoken/neg/'

## read list of positive files
pos_files = utils.get_files_path_in_folder(pos_folder)
## set 80% of files as train set and 20% as test set
pos_train_files = pos_files[:int(len(pos_files)*0.8)]
## read list of negative files
neg_files = utils.get_files_path_in_folder(neg_folder)
## set 80% of files as train set and 20% as test set
neg_train_files = neg_files[:int(len(neg_files)*0.8)]

## get vocabs from vocabs files
vocab_file_path = './data/vocabs.txt'
vocabs = utils.read_vocabs(vocab_file_path)
print('Loaded vocabs: ', len(vocabs), ' vocabs')

def cal_frequences_of_vocabs(vocabs, file_paths):
    """
    Calculating frequences of vocabs
    If word appear more than 1 time in a document,
    still count 1.
    Parameters:
        @vocabs: list of vocabulary
        @file_paths: path of training files
    Returns:
        @frequences: frequences of vocabularies
        frequences = {vocab: frequenc}
    """
    frequences = {}
    for f in file_paths:
        tokens= utils.get_tokens_in_file(f)
        if tokens is not None:
            tokens = [token for token in tokens if token in vocabs]
            tokens = set(tokens)
            for token in tokens:
                if token not in frequences:
                    frequences[token] = 1
                else:
                    frequences[token] += 1
    return frequences
    
def cal_number_of_vocabs_in_data(frequences):
    """
    Counting what vocabularies appear in positive set or negative set
    Parameters:
        frequences: frequences of vocabularies in dataset
        frequences = {word: frequence}
    Returns:
        count: number vocabularies in dataset
    """
    count = 0
    for vocab, frequence in frequences.items():
        if frequence > 0:
            count += 1
    return count
                    
## cal frequences of vocabs in positive train set
pos_frequences = cal_frequences_of_vocabs(vocabs, pos_train_files)
print('Calculated positive vocabs frequences')
## cal number of vocabs appear in positive train set
num_pos_vocabs = cal_number_of_vocabs_in_data(pos_frequences)
print(num_pos_vocabs, ' vocabs in positive training set')
## cal frequences of vocabs in negative train set
neg_frequences = cal_frequences_of_vocabs(vocabs, neg_train_files)
print('Calculated negative vocabs frequences')
## cal number of vocabs appear in negative train set
num_neg_vocabs = cal_number_of_vocabs_in_data(neg_frequences)
print(num_neg_vocabs, ' vocabs in positive training set')

## write number of positve train files and negative train files to file
number_of_sample_path = './data/number_of_sample.txt'
with open(number_of_sample_path,'w') as f:
    to_write = str(len(pos_train_files)) + ' ' + str(len(neg_train_files)) \
                + ' ' + str(num_pos_vocabs) + ' ' + str(num_neg_vocabs)
    f.write(to_write)
f.close()

def write_frequences_to_file(vocabs, frequences, file_path):
    """
    Write frquences of vocabularies to file
    Parameters:
        @vocabs: list of vocabularies
        @frequences: frequence of each vocabulary in vocabs
        @file_path: where to save frequences
    """
    to_write = ''
    for vocab in vocabs:
        if vocab not in frequences:
            to_write = to_write + '0\n'
        else:
            to_write = to_write + str(frequences[vocab]) + '\n'
    with open(file_path, 'w') as f:
        f.write(to_write)
    f.close()
    
## wirte freqences of positive vocabs to file
pos_frequences_file_path = './data/pos_frequences.txt'
write_frequences_to_file(vocabs, pos_frequences, pos_frequences_file_path)
print('Wrote positive vocabs frequences')
## wirte freqences of negative vocabs to file
neg_frequences_file_path = './data/neg_frequences.txt'
write_frequences_to_file(vocabs, neg_frequences, neg_frequences_file_path)
print('Wrote negative vocabs frequences')

